############################################
#
# Script for Siril 1.0
# April 2022
# (C) Cyril Richard
# Mono_Preprocessing v1.1
#
########### PREPROCESSING SCRIPT ###########
#
# Script for mono camera preprocessing
#
# Needs 4 sets of RAW images in the working
# directory, within 4 directories:
#   biases/
#   flats/
#   darks/
#   lights/
#
############################################

requires 1.1.0

# Convert Bias Frames to .fit files
cd biases
convert bias -out=../process
cd ../process

# Stack Bias Frames to bias_stacked.fit
stack bias rej 3 3 -nonorm
cd ..

# Convert Flat Frames to .fit files
cd flats
convert flat -out=../process
cd ../process

# Pre-process Flat Frames
preprocess flat -bias=bias_stacked

# Stack Flat Frames to pp_flat_stacked.fit
stack pp_flat rej 3 3 -norm=mul
cd ..

# Convert Dark Frames to .fit files
cd darks
convert dark -out=../process
cd ../process

# Stack Dark Frames to dark_stacked.fit
stack dark rej 3 3 -nonorm
cd ..

# Convert Light Frames to .fit files
cd lights
convert light -out=../process
cd ../process

# Pre-process Light Frames
preprocess light -dark=dark_stacked -flat=pp_flat_stacked -cc=dark

# Align Lights
register pp_light

# Stack calibrated lights to result.fit
stack r_pp_light rej 3 3 -norm=addscale -output_norm -out=../result

cd ..
close
